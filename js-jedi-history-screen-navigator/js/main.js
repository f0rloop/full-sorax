// Display Navigator Object with its Properties
// console.log(navigator);

// Display browser history length
// console.log(history.length);

// Navigate Browser History - back / forward 
// history.forward();
// history.back();

var links, 
	updatestate, 
	contentEl,
	navEl;

contentEl = document.querySelector('.content');
navEl = document.querySelector('.nav');

links = {
	main: "This is the <strong>main</strong> page",
	about: "This is the <strong>about</strong> page",
	downloads: "This is the <strong>downloads</strong> page",
	contact: "This is the <strong>contact</strong> page"
};

updatestate = function (state) {
	if (!state) return;
	contentEl.innerHTML = links[state.page];
};

window.addEventListener('popstate', function(e) {
	updatestate(e.state);
})

window.addEventListener('hashchange', updatestate);
window.addEventListener('load', updatestate);

navEl.addEventListener('click', function(e) {
	var state;
	if (e.target.tagName !== 'A') return;
	state = { page: e.target.getAttribute('href') };
	history.pushState(state, '', state.page);
	updatestate(state);
	e.preventDefault();
})